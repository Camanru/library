<?php

/**
 * My print_r - debug function
 *
 * @param $text *
 * Usage example: mpr($mixed);
 */
if (!function_exists('mpr')) {
    function mpr($text)
    {
        echo "<pre style='text-align: left;'>";
        print_r($text);
        echo "</pre>";
    }
}

/**
 * My var_dump - debug function
 *
 * @param $text *
 * Usage example: mvd($mixed);
 */
if (!function_exists('mvd')) {
    function mvd($text)
    {
        echo "<pre style='text-align: left;'>";
        var_dump($text);
        echo "</pre>";
    }
}

/**
 * My print_r and die
 *
 * @param $text *
 * Usage example: mvd($mixed);
 */
if (!function_exists('dd')) {
    function dd($text)
    {
        mpr($text);
        die;
    }
}

/**
 * My htmlspecialchars
 *
 * @param $text
 * @return string
 */
if (!function_exists('encode')) {
    function encode($text)
    {
        return htmlspecialchars(trim($text), ENT_QUOTES, "UTF-8");
    }
}

/**
 * My htmlspecialchars_decode
 *
 * @param $text
 * @return string
 */
if (!function_exists('decode')) {
    function decode($text)
    {
        return htmlspecialchars_decode($text, ENT_QUOTES);
    }
}

/**
 * Convert string ro array
 *
 * @param        $string
 * @param string $delimiter
 * @return array
 */
if (!function_exists('str2arr')) {
    function str2arr($string, $delimiter = ',')
    {
        if (trim($string)) {
            return explode($delimiter, $string);
        } else {
            return [];
        }
    }
}

/**
 * Convert array to string
 *
 * @param array $array
 * @param string $delimiter
 * @return string
 */
if (!function_exists('arr2str')) {
    function arr2str(array $array, $delimiter = ',')
    {
        foreach ($array as $k => $v) {
            if ($v == '') {
                unset($array[$k]);
                continue;
            }
            if ($v != '' and $delimiter == ',') {
                $array[$k] = str_replace(",", "&#044;", $v);
            }
        }
        return implode($delimiter, $array);
    }
}

/**
 * Convert array to string
 *
 * @param array $array
 * @param string $delimiter
 * @param boolean $recursion
 */
if (!function_exists('array2string')) {
    function array2string(array $array, $delimiter = '/', $recursion = false)
    {
        $url = '';
        foreach ($array as $v) {
            if (!is_array($v) and $v != '') {
                $url .= $delimiter . $v;
            } else {
                if ($recursion == true and is_array($v)) {
                    $url .= array2string($v);
                }
            }
        }
        return $url;
    }
}

/**
 * Convert XML to Array
 *
 * @param $xml
 * @return mixed
 */
if (!function_exists('xml2arr')) {
    function xml2arr($xml)
    {
        $simplexml = simplexml_load_string($xml);
        $json = json_encode($simplexml);
        $array = json_decode($json, true);
        return $array;
    }
}

/**
 * Translate rus in eng
 *
 * @param            $text
 * @param bool|false $upper
 * @return mixed|string
 */
if (!function_exists('translate')) {
    function translate($text, $upper = false)
    {
        if ($upper == false) {
            $text = mb_strtolower($text);
        }
        $text = trim($text);
        $text = strip_tags($text);
        $arr = [
            'дж' => 'j', ' ' => '-', 'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'i', 'к' => 'k', 'л' => 'l', 'м' => 'm',
            'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shc', 'ъ' => '', 'ы' => 'y', 'ь' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ДЖ' => 'J', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I', 'Й' => 'I', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'TS', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHC', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA'
        ];
        $in = array_keys($arr);
        $ou = array_values($arr);
        $text = str_replace($in, $ou, $text);
        $text = str_replace("--", "-", $text);
        $text = preg_replace("/[^A-z0-9_.\-\/]/", "", $text);
        return $text;
    }
}

/**
 * Files list in folder
 *
 * @param            $dir
 * @param bool|false $path
 * @return array
 */
if (!function_exists('files')) {
    function files($dir, $path = false, $extension = [])
    {
        $return = [];
        if (($opendir = @opendir($dir)) == false) {
            return false;
        }
        while (($file = readdir($opendir)) !== false) {
            if (is_file($dir . "/" . $file) && $file != "." && $file != ".." && $file != "_thumbs") {
                $ext = pathinfo($dir . "/" . $file, PATHINFO_EXTENSION);
                if (count($extension) == 0 or in_array($ext, $extension)) {
                    if ($path == false) {
                        $return[] = $file;
                    } else {
                        $return[] = $dir . $file;
                    }
                }
            }
        }
        sort($return, SORT_STRING);
        return $return;
    }
}


/**
 * Size folder
 *
 * @param $dir
 * @return int
 */
if (!function_exists('dir_size')) {
    function dir_size($dir)
    {
        $totalsize = 0;
        if ($dirstream = opendir($dir)) {
            while ($filename = readdir($dirstream)) {
                if (($filename != ".") && ($filename != "..") && ($filename != ".htaccess")) {
                    if (is_file($dir . "/" . $filename)) {
                        $totalsize += filesize($dir . "/" . $filename);
                    }
                    if (is_dir($dir . "/" . $filename)) {
                        $totalsize += dir_size($dir . "/" . $filename);
                    }
                }
            }
            closedir($dirstream);
        }
        return $totalsize;
    }
}

/**
 * Folders list in dir
 *
 * @param $dir
 * @return array|bool
 */
if (!function_exists('dirs')) {
    function dirs($dir)
    {
        if (is_dir($dir)) {
            $opendir = opendir($dir);
            while (($folder = readdir($opendir)) !== false) {
                if (is_dir($dir . "/" . $folder) && $folder != "." && $folder != ".." && $folder != "_thumbs") {
                    $return[] = $folder;
                }
            }
            if (isset($return) and count($return) > 0) {
                return $return;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

/**
 * Folder cleaner
 *
 * @param $dir
 * @return bool
 */
if (!function_exists('cleaner')) {
    function cleaner($dir)
    {
        if ($dirstream = opendir($dir)) {
            while ($filename = readdir($dirstream)) {
                if (($filename != ".") && ($filename != "..")) {
                    if (is_file($dir . "/" . $filename)) {
                        if (!unlink($dir . "/" . $filename)) {
                            return false;
                        }
                    }
                    if (is_dir($dir . "/" . $filename)) {
                        cleaner($dir . "/" . $filename);
                        if (!rmdir($dir . "/" . $filename)) {
                            return false;
                        }
                    }
                }
            }
            closedir($dirstream);
            return true;
        } else {
            return false;
        }
    }
}

/**
 * My mkdir
 *
 * @param $dir
 * @return bool
 */
if (!function_exists('new_dir')) {
    function new_dir($dir)
    {
        if (!is_dir($dir)) {
            if ((mkdir($dir, 0777)) === false) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}

/**
 * Convert date to timestamp
 *
 * @param $date
 * @return false|int
 */
if (!function_exists('date2time')) {
    function date2time($date)
    {
        return strtotime($date);
    }
}

/**
 * Convert datetime to date
 *
 * @param $date
 * @return false|string
 */
if (!function_exists('date2date')) {
    function date2date($date)
    {
        return date("Y-m-d H:i:s", strtotime($date));
    }
}

/**
 * Convert datetime to only date
 *
 * @param $date
 * @return false|string
 */
if (!function_exists('date2onlyDate')) {
    function date2onlyDate($date)
    {
        return date("Y-m-d", strtotime($date));
    }
}

/**
 * Convert timestamp to date
 *
 * @param $time
 * @return false|string
 */
if (!function_exists('time2date')) {
    function time2date($time)
    {
        return date("Y-m-d H:i:s", $time);
    }
}

/**
 * Range date
 *
 * @param $date
 * @return array
 */
if (!function_exists('range2date')) {
    function range2date($date)
    {
        $date = date2date($date);
        return [
            date("Y-m-d 00:00:00", strtotime($date)),
            date("Y-m-d 23:59:59", strtotime($date)),
        ];
    }
}

#
/**
 * Date to day
 *
 * @param $datetime
 * @return string
 */
if (!function_exists('date2day')) {
    function date2day($datetime)
    {
        $time = date2time($datetime);
        $date = date2onlyDate($datetime);

        $now = date("Y-m-d");
        $tomorrow = date("Y-m-d", strtotime('+1 day', strtotime($now)));
        $yesterday = date("Y-m-d", strtotime('-1 day', strtotime($now)));

        if ($date == $now) {
            $str = "Сегодня";
        } elseif ($date == $tomorrow) {
            $str = "Завтра";
        } elseif ($date == $yesterday) {
            $str = "Вчера";
        } else {
            $str = date("d.m.Y", $time);
        }

        return $str . " в " . (date("H:i", $time));
    }
}

/**
 * Date to month
 */
if (!function_exists('date2month')) {
    function date2month($datetime)
    {
        $months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $month = date("m", date2time($datetime)) - 1;
        return date("j {$months[$month]} Y", date2time($datetime));
    }
}

/**
 * @param $password
 * @return string
 */
if (!function_exists('hashPassword')) {
    function hashPassword($password)
    {
        return md5(strrev(md5($password)));
    }
}

/**
 * @param $phone
 * @return bool|mixed|string
 */
if (!function_exists('phone')) {
    function phone($phone)
    {
        $phone = trim($phone);
        $phone = preg_replace("/[^0-9]/", "", $phone);

        if (strlen($phone) == 10) {
            $phone = "7" . $phone;
        } elseif (strlen($phone) == 11) {
            $phone = "7" . substr($phone, 1, 10);
        } elseif (strlen($phone) == 12) {
            $phone = "7" . substr($phone, 2, 10);
        } else {
            $phone = false;
        }

        return $phone;
    }
}

/**
 * @param array $post
 * @return array
 */
if (!function_exists('encodePost')) {
    function encodePost(array $post)
    {
        $row = [];
        foreach ($post as $k => $v) {
            $k = encode($k);

            if (!is_array($post[$k])) {
                $row[$k] = encode($v);
            } else {
                $row[$k] = arr2str($v);
            }
        }
        return $row;
    }
}

/**
 * @param array $post
 * @param string $key
 * @return array|bool
 */
if (!function_exists('keyId')) {
    function keyId(array $post, $key = 'id')
    {
        $rows = [];
        foreach ($post as $v) {
            if (!isset($v[$key])) {
                return false;
            } else {
                $rows[$v[$key]] = $v;
            }
        }
        return $rows;
    }
}

/**
 * @param array $post
 * @return array
 */
if (!function_exists('keyValue')) {
    function keyValue(array $post)
    {
        $rows = [];
        foreach ($post as $v) {
            $rows[$v] = $v;
        }
        return $rows;
    }
}

/**
 * @param $email
 * @return bool
 */
if (!function_exists('isEmail')) {
    function isEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
            return false;
        } else {
            return true;
        }
    }
}

/**
 * @param $phone
 * @param int $countCityLen
 * @return mixed|string
 */
if (!function_exists('hashPhone')) {
    function hashPhone($phone, $countCityLen = 6)
    {
        $hash = hashPassword(phone($phone));
        $phone = preg_replace("/[^0-9]/", "", $hash);
        $phone = substr($phone, 0, $countCityLen);
        return $phone;
    }
}

/**
 * Число в мегабайтах
 */
if (!function_exists('number2string')) {
    function number2string($number)
    {
        $formats = array('', 'K', 'M', 'G', 'T');
        $format = 0;

        while ($number >= 1000 && count($formats) != ++$format) {
            $number = round($number / 1000, 2);
        }
        $formats[] = 'T';

        return $number . $formats[$format];
    }
}

/**
 * Http response 404
 *
 * @param array $template
 * @return array
 */
if (!function_exists('response404')) {
    function response404(array $template = ['404'])
    {
        $php_sapi_name = php_sapi_name();
        if ($php_sapi_name == 'cgi' or $php_sapi_name == 'cgi-fcgi') {
            header('Status: 404 Not Found');
        } else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        }
        return $template;
    }
}

/**
 * Http response 503
 *
 * @param array $template
 * @return array
 */
if (!function_exists('response503')) {
    function response503(array $template = ['503'])
    {
        $php_sapi_name = php_sapi_name();
        if ($php_sapi_name == 'cgi' or $php_sapi_name == 'cgi-fcgi') {
            header('Status: 503 Service Unavailable');
        } else {
            header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
        }
        header('Retry-After: 86400');
        return $template;
    }
}

/**
 * Функция поиска полного url для статической страницы
 * В функцию передается массив всех страниц, где:
 * название страницы соответствует столбцу $pageName
 * родительский элемент соответствует столбцу $pageParent
 *
 * @param $pages
 * @param $id
 * @param array $url
 * @param string $pageName
 * @param string $pageParent
 * @return array
 */
if (!function_exists('pathUrl')) {
    function pathUrl($pages, $id, array $url = [], $pageName = 'engname', $pageParent = 'parent')
    {
        foreach ($pages as $page) {
            if ($page['id'] == $id) {
                $url[] = $page[$pageName];
                if ($page[$pageParent] > 0) {
                    $url = pathUrl($pages, $page[$pageParent], $url);
                }
            }
        }
        return $url;
    }
}

/**
 * Получаем экземпляр запроса по ключу
 * @param $key
 * @param null $default
 * @return mixed
 */
if (!function_exists('request')) {
    function request($key = null, $default = null)
    {
        if ($key != null) {
            return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
        }
        return $_REQUEST;
    }
}

/**
 * Проверка существования переменной
 * @param $var
 * @param bool $default
 * @return bool
 */
if (!function_exists('exist')) {
    function exist(&$var, $default = false)
    {
        if ($var or (is_scalar($var) and strlen($var))) {
            return $var;
        }
        return $default;
    }
}